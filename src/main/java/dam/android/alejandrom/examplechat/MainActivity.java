package dam.android.alejandrom.examplechat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public  class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv;
    EditText et;
    Button button;
    PrintWriter pw;
    BufferedReader br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            setUI();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void setUI() throws IOException {
        tv = findViewById(R.id.textView2);
        et = findViewById(R.id.editTextTextPersonName);
        button = findViewById(R.id.button);

        Socket socket = new Socket("137.74.226.41", 9090);

        pw = new PrintWriter(socket.getOutputStream(), true);
        br = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        button.setOnClickListener(this);

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                do {
                    readLines();
                } while (socket.isConnected());
            }
        });
        thread1.start();
    }

    private void readLines() {
        String line = "";
        try {
            if((line = br.readLine()) != null){
                Handler handler = new Handler(Looper.getMainLooper());
                String finalLine = line;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        tv.setText(finalLine);
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                pw.println(et.getText().toString());
            }
        });
        thread.start();
    }
}